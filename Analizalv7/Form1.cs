﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Analizalv7
{
    public partial class Form1 : Form
    {
        bool potez = true;
        int brojpoteza = 0;
        int rezO = 0;
        int rezX = 0;
        
        
        

        public Form1()
        {
            InitializeComponent();
            disablebuttons();
        }

        private void p11_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;

            if (potez == false)
                napotezu.Text = ime1.Text+" na potezu";
            else if (potez == true)
                napotezu.Text = ime2.Text+" na potezu";

            if (potez == true)
                b.Text = "X";
            else
                b.Text = "O";

            potez = !potez;
            b.Enabled = false;

            pobjeda();

        }

        public void pobjeda()
        {
            bool pobjednik = false;
            if ((p11.Text == p12.Text) && (p12.Text == p13.Text) && !p11.Enabled)
                pobjednik = true;

            else if ((p21.Text == p22.Text) && (p22.Text == p23.Text) && !p21.Enabled)
                pobjednik = true;

            else if ((p31.Text == p32.Text) && (p32.Text == p33.Text) && !p31.Enabled)
                pobjednik = true;

            else if ((p11.Text == p21.Text) && (p21.Text == p31.Text) && !p11.Enabled)
                pobjednik = true;

            else if ((p12.Text == p22.Text) && (p22.Text == p32.Text) && !p12.Enabled)
                pobjednik = true;

            else if ((p13.Text == p23.Text) && (p23.Text == p33.Text) && !p13.Enabled)
                pobjednik = true;

            else if ((p11.Text == p22.Text) && (p22.Text == p33.Text) && !p11.Enabled)
                pobjednik = true;

            else if ((p13.Text == p22.Text) && (p22.Text == p31.Text) && !p13.Enabled)
                pobjednik = true;

            if(pobjednik)
            {
                disablebuttons();
                napotezu.Text = "";
                if (potez != true)
                {
                    MessageBox.Show(ime1.Text+" je pobjedio!", "Win!");
                    rezX += 1;
                }

                else if (potez == true)
                {
                    MessageBox.Show(ime2.Text+" je pobjedio!", "Win!");
                    rezO += 1;
                }

                rezolabel.Text = rezO.ToString();
                rezxlabel.Text = rezX.ToString();
            }

            if (p11.Enabled == false && p12.Enabled == false && p13.Enabled == false && p21.Enabled == false && p22.Enabled == false && p23.Enabled == false && p31.Enabled == false && p32.Enabled == false && p33.Enabled == false && pobjednik==false)
            {
                MessageBox.Show("Neriješeno je!", "Rezultat");
            }

        }

        private void label1_Click(object sender, EventArgs e)
        {
            
        }

        private void butImena_Click(object sender, EventArgs e)
        {
            imeigrac1.Text = ime1.Text;
            imeigrac2.Text = ime2.Text;
            
        }

        private void newgame_Click(object sender, EventArgs e)
        {
            enablebuttons();
            rezxlabel.Text = rezX.ToString();
            rezolabel.Text = rezO.ToString();
            if (ime1.Text == "")
            {
                ime1.Text = "X";
                imeigrac1.Text = "X";
            }
            if (ime2.Text == "")
            {
                ime2.Text = "O";
                imeigrac2.Text = "O";
            }
            if (potez == true)
            napotezu.Text = ime1.Text + " na potezu";

            if (potez == false)
            napotezu.Text = ime2.Text + " na potezu";
        }
        private void disablebuttons()
        {
            p11.Enabled = false;
            p12.Enabled = false;
            p13.Enabled = false;
            p21.Enabled = false;
            p22.Enabled = false;
            p23.Enabled = false;
            p31.Enabled = false;
            p32.Enabled = false;
            p33.Enabled = false;
        }

        private void enablebuttons()
        {
            p11.Text = "";
            p12.Text = "";
            p13.Text = "";
            p21.Text = "";
            p22.Text = "";
            p23.Text = "";
            p31.Text = "";
            p32.Text = "";
            p33.Text = "";
            p11.Enabled = true;
            p12.Enabled = true;
            p13.Enabled = true;
            p21.Enabled = true;
            p22.Enabled = true;
            p23.Enabled = true;
            p31.Enabled = true;
            p32.Enabled = true;
            p33.Enabled = true;
        }

        private void restart_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        private void izlaz_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}

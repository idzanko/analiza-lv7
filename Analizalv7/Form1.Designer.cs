﻿namespace Analizalv7
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.newgame = new System.Windows.Forms.Button();
            this.p11 = new System.Windows.Forms.Button();
            this.p12 = new System.Windows.Forms.Button();
            this.p13 = new System.Windows.Forms.Button();
            this.p21 = new System.Windows.Forms.Button();
            this.p22 = new System.Windows.Forms.Button();
            this.p23 = new System.Windows.Forms.Button();
            this.p31 = new System.Windows.Forms.Button();
            this.p32 = new System.Windows.Forms.Button();
            this.p33 = new System.Windows.Forms.Button();
            this.ime1 = new System.Windows.Forms.TextBox();
            this.ime2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.butImena = new System.Windows.Forms.Button();
            this.imeigrac1 = new System.Windows.Forms.Label();
            this.imeigrac2 = new System.Windows.Forms.Label();
            this.rezxlabel = new System.Windows.Forms.Label();
            this.rezolabel = new System.Windows.Forms.Label();
            this.napotezu = new System.Windows.Forms.Label();
            this.restart = new System.Windows.Forms.Button();
            this.izlaz = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // newgame
            // 
            this.newgame.Location = new System.Drawing.Point(12, 12);
            this.newgame.Name = "newgame";
            this.newgame.Size = new System.Drawing.Size(75, 23);
            this.newgame.TabIndex = 0;
            this.newgame.Text = "Nova igra";
            this.newgame.UseVisualStyleBackColor = true;
            this.newgame.Click += new System.EventHandler(this.newgame_Click);
            // 
            // p11
            // 
            this.p11.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.p11.Location = new System.Drawing.Point(12, 41);
            this.p11.Name = "p11";
            this.p11.Size = new System.Drawing.Size(100, 100);
            this.p11.TabIndex = 1;
            this.p11.UseVisualStyleBackColor = true;
            this.p11.Click += new System.EventHandler(this.p11_Click);
            // 
            // p12
            // 
            this.p12.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.p12.Location = new System.Drawing.Point(118, 41);
            this.p12.Name = "p12";
            this.p12.Size = new System.Drawing.Size(100, 100);
            this.p12.TabIndex = 2;
            this.p12.UseVisualStyleBackColor = true;
            this.p12.Click += new System.EventHandler(this.p11_Click);
            // 
            // p13
            // 
            this.p13.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.p13.Location = new System.Drawing.Point(224, 41);
            this.p13.Name = "p13";
            this.p13.Size = new System.Drawing.Size(100, 100);
            this.p13.TabIndex = 3;
            this.p13.UseVisualStyleBackColor = true;
            this.p13.Click += new System.EventHandler(this.p11_Click);
            // 
            // p21
            // 
            this.p21.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.p21.Location = new System.Drawing.Point(12, 147);
            this.p21.Name = "p21";
            this.p21.Size = new System.Drawing.Size(100, 100);
            this.p21.TabIndex = 4;
            this.p21.UseVisualStyleBackColor = true;
            this.p21.Click += new System.EventHandler(this.p11_Click);
            // 
            // p22
            // 
            this.p22.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.p22.Location = new System.Drawing.Point(118, 147);
            this.p22.Name = "p22";
            this.p22.Size = new System.Drawing.Size(100, 100);
            this.p22.TabIndex = 5;
            this.p22.UseVisualStyleBackColor = true;
            this.p22.Click += new System.EventHandler(this.p11_Click);
            // 
            // p23
            // 
            this.p23.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.p23.Location = new System.Drawing.Point(224, 147);
            this.p23.Name = "p23";
            this.p23.Size = new System.Drawing.Size(100, 100);
            this.p23.TabIndex = 6;
            this.p23.UseVisualStyleBackColor = true;
            this.p23.Click += new System.EventHandler(this.p11_Click);
            // 
            // p31
            // 
            this.p31.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.p31.Location = new System.Drawing.Point(12, 253);
            this.p31.Name = "p31";
            this.p31.Size = new System.Drawing.Size(100, 100);
            this.p31.TabIndex = 7;
            this.p31.UseVisualStyleBackColor = true;
            this.p31.Click += new System.EventHandler(this.p11_Click);
            // 
            // p32
            // 
            this.p32.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.p32.Location = new System.Drawing.Point(118, 253);
            this.p32.Name = "p32";
            this.p32.Size = new System.Drawing.Size(100, 100);
            this.p32.TabIndex = 8;
            this.p32.UseVisualStyleBackColor = true;
            this.p32.Click += new System.EventHandler(this.p11_Click);
            // 
            // p33
            // 
            this.p33.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.p33.Location = new System.Drawing.Point(224, 253);
            this.p33.Name = "p33";
            this.p33.Size = new System.Drawing.Size(100, 100);
            this.p33.TabIndex = 9;
            this.p33.UseVisualStyleBackColor = true;
            this.p33.Click += new System.EventHandler(this.p11_Click);
            // 
            // ime1
            // 
            this.ime1.Location = new System.Drawing.Point(393, 38);
            this.ime1.Name = "ime1";
            this.ime1.Size = new System.Drawing.Size(100, 20);
            this.ime1.TabIndex = 10;
            // 
            // ime2
            // 
            this.ime2.Location = new System.Drawing.Point(393, 69);
            this.ime2.Name = "ime2";
            this.ime2.Size = new System.Drawing.Size(100, 20);
            this.ime2.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(344, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Igrač 1:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(344, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Igrač 2:";
            // 
            // butImena
            // 
            this.butImena.Location = new System.Drawing.Point(499, 36);
            this.butImena.Name = "butImena";
            this.butImena.Size = new System.Drawing.Size(75, 53);
            this.butImena.TabIndex = 14;
            this.butImena.Text = "Unesi";
            this.butImena.UseVisualStyleBackColor = true;
            this.butImena.Click += new System.EventHandler(this.butImena_Click);
            // 
            // imeigrac1
            // 
            this.imeigrac1.AutoSize = true;
            this.imeigrac1.Location = new System.Drawing.Point(347, 134);
            this.imeigrac1.Name = "imeigrac1";
            this.imeigrac1.Size = new System.Drawing.Size(30, 13);
            this.imeigrac1.TabIndex = 15;
            this.imeigrac1.Text = "Ime1";
            // 
            // imeigrac2
            // 
            this.imeigrac2.AutoSize = true;
            this.imeigrac2.Location = new System.Drawing.Point(347, 163);
            this.imeigrac2.Name = "imeigrac2";
            this.imeigrac2.Size = new System.Drawing.Size(30, 13);
            this.imeigrac2.TabIndex = 16;
            this.imeigrac2.Text = "Ime2";
            // 
            // rezxlabel
            // 
            this.rezxlabel.AutoSize = true;
            this.rezxlabel.Location = new System.Drawing.Point(393, 134);
            this.rezxlabel.Name = "rezxlabel";
            this.rezxlabel.Size = new System.Drawing.Size(0, 13);
            this.rezxlabel.TabIndex = 17;
            // 
            // rezolabel
            // 
            this.rezolabel.AutoSize = true;
            this.rezolabel.Location = new System.Drawing.Point(393, 163);
            this.rezolabel.Name = "rezolabel";
            this.rezolabel.Size = new System.Drawing.Size(0, 13);
            this.rezolabel.TabIndex = 18;
            // 
            // napotezu
            // 
            this.napotezu.AutoSize = true;
            this.napotezu.Location = new System.Drawing.Point(194, 17);
            this.napotezu.Name = "napotezu";
            this.napotezu.Size = new System.Drawing.Size(0, 13);
            this.napotezu.TabIndex = 19;
            // 
            // restart
            // 
            this.restart.Location = new System.Drawing.Point(347, 329);
            this.restart.Name = "restart";
            this.restart.Size = new System.Drawing.Size(75, 23);
            this.restart.TabIndex = 20;
            this.restart.Text = "Restart";
            this.restart.UseVisualStyleBackColor = true;
            this.restart.Click += new System.EventHandler(this.restart_Click);
            // 
            // izlaz
            // 
            this.izlaz.Location = new System.Drawing.Point(428, 329);
            this.izlaz.Name = "izlaz";
            this.izlaz.Size = new System.Drawing.Size(75, 23);
            this.izlaz.TabIndex = 21;
            this.izlaz.Text = "Izlaz";
            this.izlaz.UseVisualStyleBackColor = true;
            this.izlaz.Click += new System.EventHandler(this.izlaz_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(589, 377);
            this.Controls.Add(this.izlaz);
            this.Controls.Add(this.restart);
            this.Controls.Add(this.napotezu);
            this.Controls.Add(this.rezolabel);
            this.Controls.Add(this.rezxlabel);
            this.Controls.Add(this.imeigrac2);
            this.Controls.Add(this.imeigrac1);
            this.Controls.Add(this.butImena);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ime2);
            this.Controls.Add(this.ime1);
            this.Controls.Add(this.p33);
            this.Controls.Add(this.p32);
            this.Controls.Add(this.p31);
            this.Controls.Add(this.p23);
            this.Controls.Add(this.p22);
            this.Controls.Add(this.p21);
            this.Controls.Add(this.p13);
            this.Controls.Add(this.p12);
            this.Controls.Add(this.p11);
            this.Controls.Add(this.newgame);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Iks Oks";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button newgame;
        private System.Windows.Forms.Button p11;
        private System.Windows.Forms.Button p12;
        private System.Windows.Forms.Button p13;
        private System.Windows.Forms.Button p21;
        private System.Windows.Forms.Button p22;
        private System.Windows.Forms.Button p23;
        private System.Windows.Forms.Button p31;
        private System.Windows.Forms.Button p32;
        private System.Windows.Forms.Button p33;
        private System.Windows.Forms.TextBox ime1;
        private System.Windows.Forms.TextBox ime2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button butImena;
        private System.Windows.Forms.Label imeigrac1;
        private System.Windows.Forms.Label imeigrac2;
        private System.Windows.Forms.Label rezxlabel;
        private System.Windows.Forms.Label rezolabel;
        private System.Windows.Forms.Label napotezu;
        private System.Windows.Forms.Button restart;
        private System.Windows.Forms.Button izlaz;
    }
}

